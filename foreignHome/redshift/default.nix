{...}: {
  services.redshift = {
    enable = true;
    tray = true;
    temperature.day = 6000;
    temperature.night = 4500;
    dawnTime = "5:00-6:45";
    duskTime = "16:35-20:15";
    provider = "manual";
    settings = {
      redshift = {
        fade = "1";
        gamma = "1";
        brightness-day = "1";
        brightness-night = "0.8";
        adjustment-method = "randr";
      };
    };
  };
}
