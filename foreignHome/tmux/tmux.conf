## Tmux configuration file




# [ section configs ]


#fix colors in vim
set-option -sa terminal-overrides ",xterm*:Tc"
set -g default-terminal "xterm-256color"


#mouse support
set -g mouse on
# set -g mouse-select-pane on
# set -g mouse-resize-pane on
# set -g mouse-select-window on


#remove delay for vim
set -s escape-time 0


#title
set -g set-titles on
set -g set-titles-string "#T"


#increase scrollback buffer size
set -g history-limit 50000


#tmux messages are displayed for 4 seconds
set -g display-time 4000


#tmuxline
#refresh 'status-left' and 'status-right' more often
# set -g status-interval 5
set -g status-style bg=default
set -g status-left '#(cat #{socket_path}-\#{session_id}-vimbridge)'
set -g status-left-length 90
set -g status-right '#(cat #{socket_path}-\#{session_id}-vimbridge-R)'
set -g status-right-length 90
set -g status-justify absolute-centre


#focus events enabled for terminals that support them
set -g focus-events on

#don't close on cmd exit
# set -g remain-on-exit on



# [ section keybindings ]


#use c-a as prefix
unbind C-b
set -g prefix C-a
bind C-a send-prefix


#emacs key bindings in tmux command prompt  are better than
#vi keys, even for vim users
set -g status-keys emacs


#reload config 
bind C-r source-file ~/.config/tmux/tmux.conf


#clipboard
# bind-key -T copy-mode-vi y send-keys -X copy-pipe-and-cancel "xclip -i -sel clip > /dev/null"
# bind-key p run "xclip -o -sel clip | tmux load-buffer - ; tmux paste-buffer"


#start window and pane numbering from 1
set -g base-index 1
setw -g pane-base-index 1


#kill pane without confirmation
bind-key x kill-pane


#switch between panes 
bind j select-pane -L
bind k select-pane -D
bind l select-pane -U
bind \; select-pane -R


#split window with cwd
bind-key v split-window -c '#{pane_current_path}'
bind-key h split-window -h -c '#{pane_current_path}'


#select windows
# bind 1 select-window -t :=1
# bind 2 select-window -t :=2
# bind 3 select-window -t :=3
# bind 4 select-window -t :=4
# bind 5 select-window -t :=5
# bind 6 select-window -t :=6
# bind 7 select-window -t :=7
# bind 8 select-window -t :=8
# bind 9 select-window -t :=9
# bind 0 select-window -t :=0

bind-key -n M-1 select-window -t 1
bind-key -n M-2 select-window -t 2
bind-key -n M-3 select-window -t 3
bind-key -n M-4 select-window -t 4
bind-key -n M-5 select-window -t 5
bind-key -n M-6 select-window -t 6
bind-key -n M-7 select-window -t 7
bind-key -n M-8 select-window -t 8
bind-key -n M-9 select-window -t 9


#fzf navigation between sessions
#popups are >=3.2a, and f34 ships with 3.1c
bind C-j display-popup -E "tmux list-sessions | sed -E 's/:.*$//' | grep -v \"^$(tmux display-message -p '#S')\$\" | fzf --reverse | xargs tmux switch-client -t"
# bind C-j new-window -n "session-switcher" "tmux list-sessions | sed -E 's/:.*$//' | grep -v \"^$(tmux display-message -p '#S')\$\" | fzf --reverse | xargs tmux switch-client -t"





# [ section plugins ]


#execute this before starting

#tmux plugin manager
#git clone https://github.com/tmux-plugins/tpm ~/.local/share/tmux/plugins/tpm
#Press prefix + I (capital i, as in Install) to fetch the plugin

#session manager
#pip install --user tmuxp


#change plugin path
# set-environment -g TMUX_PLUGIN_MANAGER_PATH '~/.local/share/tmux/plugins'
#plugin manager itself
# set -g @plugin 'tmux-plugins/tpm'


#simple (but dumb) session manager
# set -g @plugin 'tmux-plugins/tmux-resurrect'
#sessions data dir 
set -g @resurrect-dir '~/.config/tmux/sessions'
#save/restore sessions with tmux-resurrect
set -g @resurrect-save 'S'
set -g @resurrect-restore 'R'
#restore pane contents
set -g @resurrect-capture-pane-contents 'on'


#copying stuff
# set -g @plugin 'tmux-plugins/tmux-yank'


#search panes
# set -g @plugin 'tmux-plugins/tmux-copycat'


# run '~/.local/share/tmux/plugins/tpm/tpm'


