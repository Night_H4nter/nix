{pkgs, ...}: {
  programs.tmux = {
    enable = true;
    secureSocket = true;
    plugins = with pkgs.tmuxPlugins; [
      yank
      resurrect
    ];
    extraConfig = builtins.readFile ./tmux.conf;
  };
}
