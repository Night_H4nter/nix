""Undotree



"TODO choose the layout
let g:undotree_WindowLayout = 1
"autofocus undotree split
let g:undotree_SetFocusWhenToggle = 1
"store undo files in separate directory
if has("persistent_undo")
    let undodir="~/.undofiles"
    set undofile
endif


"toggle undotree
nnoremap <Bslash>uu <cmd>silent UndotreeToggle<cr>
