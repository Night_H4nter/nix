{...}: {
  programs.zsh = {
    enable = true;
    initExtraFirst = ''
      source ~/.config/zsh/zprofile
    '';
    initExtra = ''
      source ~/.config/zsh/zshrc
    '';
  };

  xdg.configFile.zsh = {
    source = ./config;
    recursive = true;
  };
}
