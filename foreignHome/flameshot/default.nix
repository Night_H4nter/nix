{...}: {
  services.flameshot.enable = true;

  xdg.configFile.flameshot = {
    source = ./flameshot.ini;
    recursive = true;
    target = "flameshot/flameshot.ini";
  };
}
