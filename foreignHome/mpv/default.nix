{pkgs, ...}: {
  #don't wanna vendor lock, so manage it manually
  home.packages = with pkgs; [
    mpv
  ];

  xdg.configFile.mpv = {
    source = ./config;
    recursive = true;
  };
}
