{
  description = "My homedir config";

  inputs = {
    stable.url = "github:nixos/nixpkgs/nixos-22.11";
    unstable.url = "github:nixos/nixpkgs/nixos-unstable";

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "stable";
    };

    #impermanence = {
    #  url = "github:nix-community/#impermanence";
    #  # inputs.nixpkgs.follows = "stable";
    #};

    sops = { 
      url = "github:Mic92/sops-nix";
      inputs.nixpkgs.follows = "stable";
    };
  };

  outputs = {
    self,
    stable,
    unstable,
    #impermanence,
    home-manager,
    sops,
    #stateVersion ? "23.05",
    ...
  }@inputs: let
    system = "x86_64-linux";
    pkgs = stable.legacyPackages.${system};
    mkOutputLib = import ./common/util.nix;
  in {
    homeConfigurations = {
      daniel = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;
        modules = [
          {
            home = {
              username = "daniel";
              homeDirectory = "/home/daniel";
              stateVersion = "22.11";
            };
            programs.home-manager.enable = true;
          }

          ./foreignHome/neovim
          ./foreignHome/zathura
          ./foreignHome/flameshot
          ./foreignHome/tmux
          ./foreignHome/zsh
          ./foreignHome/redshift
          ./foreignHome/distrobox
          ./foreignHome/mpv
          # ./foreignHome/awesome
        ];
      };
    };

    nixosConfigurations = {
      nixos-vm = mkOutputLib.mkSystem {
        inherit
          stable
          unstable
          home-manager
          #impermanence
          sops;
        moduleNameList = [ "awesome" "nixos-vm" "flameshot" ];
        userName = "user";
        stateVersion = "22.11";
      };

      nhdpc = stable.lib.nixosSystem {
        specialArgs = { inherit inputs; };
        modules = [
          sops.nixosModules.sops
          #impermanence.nixosModules.#impermanence
          home-manager.nixosModules.home-manager
          #impermanence.nixosModules.home-manager.#impermanence
          ./common/nix.nix
          ./hosts/nhdpc/system.nix
          ./hosts/nhdpc/home.nix
       ];
      };
    };
  

  };
}
