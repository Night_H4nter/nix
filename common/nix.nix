{ ... }:
{
  nix = { 
    settings = {
      experimental-features = [ "nix-command" "flakes" ];
      auto-optimise-store = true;
    };
    gc = {
      automatic = true;
      options = "-d --delete-older-than 14d";
      dates = "weekly";
    };
  };
  nixpkgs.config = {
    allowUnfree = true;
    allowBroken = true;
  };
}
