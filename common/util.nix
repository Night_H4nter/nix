rec {
  # this module removes the need for a ton of boilerplate code and somewhat{{{
  # simplifies deployment of multiple systems from predefined building blocks
  # that may consist of both system (a.k.a. nixos) and user (home-manager)
  # configs withou worrying of what exactly they consist of

  # this is what directory structure should look like:
  #
  # flake root
  #   system
  #     hybridModule1
  #       ...
  #       default.nix
  #     systemOnlyModule1
  #       ...
  #       default.nix
  #     ...
  #   home
  #     hybridModule1
  #       ...
  #       default.nix
  #     homeOnlyeModule1
  #       ...
  #       default.nix
  #     ...
  #   ...
  #   flake.nix
  #
  # pay attention to the module names (basically, the directory names under
  # "system" and "home" directories). they must be identical in order for them
  # to be detected by this library}}}

  # a more abstract function is too bulky, so gotta have separarte functions
  # to handle concatenation of module paths
  mkNixosSystemModulePath = name:
    ../nixosSystem + "/${name}";

  mkNixosHomeModulePath = name:
    ../nixosHome + "/${name}";

  mkForeignHomeModulePath = name:
    ../foreignHome + "/${name}";

  trace = builtins.trace;

  # check if modules specified exists
  moduleExists = path:
    trace path
    (if builtins.pathExists path
    then trace "module exists" path
    else trace "module does not exist" null);

  # given a pseudo hybrid module list, build an attrset of system and home
  # modules from it
  mkModuleLists = { pkgs, nameList ? [] }:
  let
    remove = pkgs.lib.lists.remove;
  in {
    nixosSystemModuleList =
      remove null
        (map moduleExists
          (map mkNixosSystemModulePath nameList));
    nixosHomeModules =
      remove null
        (map moduleExists
          (map mkNixosHomeModulePath nameList));
    foreignHomeModules =
      remove null
        (map moduleExists
          (map mkForeignHomeModulePath nameList));
  };

  # assemble a full system object
  mkSystem = inputs@{
    stable,
    unstable,
    #impermanence,
    home-manager,
    sops,
    hostName ? "nhdpc",
    userName ? "nh",
    moduleNameList ? [],
    stateVersion ? "22.11",
    ...
  }:
  let
    lib = stable.lib;
    singleton = lib.lists.singleton;
    moduleSet = mkModuleLists 
      { pkgs = stable; nameList = moduleNameList; };
  in
    stable.lib.nixosSystem {
      system = "x86_64-linux";
      modules = singleton home-manager.nixosModules.home-manager
        ++ singleton ./nix.nix
        ++ moduleSet.nixosSystemModuleList
        ++ singleton ../users/${userName}.nix
        ++ singleton {
          home-manager = {
            useGlobalPkgs = true;
            useUserPackages = true;
            extraSpecialArgs = { inherit inputs; };
            users.${userName} = {
              home = {
                username = "${userName}";
                homeDirectory = "/home/${userName}";
                inherit stateVersion;
              };
              imports = moduleSet.foreignHomeModules
                ++ moduleSet.nixosHomeModules;
            };
          };
        };
      specialArgs = { inherit inputs; };
    };
/*{{{*/
  
  # mkNixosUser = inputs@{
  #   stable,
  #   unstable,
  #   #impermanence,
  #   home-manager,
  #   sops,
  #   host ? "nhdpc",
  #   userName ? "nh",
  #   # moduleNameList ? [],
  #   stateVersion ? "22.11",
  #   system ? "x86_64-linux",
  #   ...
  # }:
  # let
  #   lib = stable.lib;
  #   singleton = lib.lists.singleton;
  # in
  # {
  #   users.${userName} = {
  #     home = {
  #       username = "${userName}";
  #       homeDirectory = "/home/${userName}";
  #       inherit stateVersion;
  #     };
  #   };
  # };



  mkSystemkew = {
    stable,
    unstable,
    #impermanence,
    home-manager,
    sops,
    # host ? "nhdpc",
    # userName ? "nh",
    stateVersion ? "22.11",
    system ? "x86_64-linux",
    ...
  }@inputs:
  let
    lib = stable.lib;
    singleton = lib.lists.singleton;
  in
    stable.lib.nixosSystem {
      inherit system;
      specialArgs = { inherit inputs; };
      modules = singleton home-manager.nixosModules.home-manager
        ++ singleton sops.nixosModules.sops
        ++ singleton #impermanence.nixosModules.#impermanence
        ++ singleton ./nix.nix
        ++ singleton ../hosts/${host}/system
        ++ singleton ../hosts/${host}/user
        ;
    };/*}}}*/
}
