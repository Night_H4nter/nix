{ stable, unstable, home-manager, ... }:
{
  imports =
    [ 
      ./hardware.nix
    ];

  boot = {
    # initrd.postDeviceCommands = stable.lib.mkAfter ''
    #   zfs rollback -r rootpool/temp/root@blank
    #   zfs rollback -r rootpool/temp/swap@blank
    # '';
    loader.grub = {
      enable = true;
      devices = [ "nodev" ];
      efiInstallAsRemovable = true;
      efiSupport = true;
    };
    supportedFilesystems = [ "zfs" ];
    zfs.requestEncryptionCredentials = true;
  };

  services.zfs.autoScrub.enable = true;
  

  services.xserver.videoDrivers = [ "nvidia" ];
  hardware = { 
    opengl = { 
      enable = true;
      driSupport32Bit = true;
    };
    pulseaudio.support32Bit = true;
  };


  networking = {
    hostName = "nhdpc";
    firewall = {
      enable = true;
      # allowedTCPPorts = [];
      # allowedUDPPorts = [];
    };
    hostId = "09f23e7a";
  };

  time.timeZone = "Europe/Saratov";
  i18n.defaultLocale = "en_US.UTF-8";
  
  systemd.extraConfig = ''
    DefaultTimeoutStartSec=10s
    DefaultTimeoutStopSec=10s
    DefaultTimeoutAbortSec=10s
    DefaultRestartSec=15ms
  '';

  #tmp
  # services.xserver.displayManager.gdm.enable = true;

  services.xserver = {
    enable = true;
    # xrandrHeads = [
    # ];
    layout = "us,ru,il";
  };









  system.copySystemConfiguration = true;
}

