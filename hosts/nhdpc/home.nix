#boilerplate {{{
{
  stable,
  unstable,
  #impermanence,
  home-manager,
  sops,
  stateVersion,
  system,
  ...
}@inputs: {
#}}}

  #boilerplate code for system-level configuration {{{
  specialArgs = { inherit inputs; };
  #}}}

  #system-level user configuration
  users.users.nh = {
    isNormalUser = true;
    description = "Daniel";
    extraGroups = [
      "networkmanager"
      "wheel"
      "kvm"
      "libvirtd"
      "adbusers"
    ];
    initialPassword = "12";
    # shell = pkgs.zsh;
    home = "/nh";
    # subUidRanges = [{ startUid = 100000; count = 65536; }];
    # subGidRanges = [{ startGid = 100000; count = 65536; }];
  };
  users.extraGroups.vboxusers.members = ["nh"];
  imports = [
  ];

  # boilerplate for home-manager{{{
  # TODO:maybe extract it into a function in utils
  home-manager = {
    useGlobalPkgs = true;
    useUserPackages = true;
    #programs.home-manager.enable = true;
    #this may be problematic down the road, idk
    extraSpecialArgs = { inherit inputs; };
    users.nh.home = { inherit stateVersion; };
  };
  #needed for #impermanence
  #programs.fuse.userAllowOther = true;
  # }}}

  #the actual home-manager configuration
  home-manager.users.nh.home = {
    username = "nh";
    homeDirectory = "/nh";
    modules = [
      ../../nixosHome/awesome
      ../../foreignHome/zsh
      ../../foreignHome/neovim
    ];
  #  persistence."/persistence/user/nh" = {
  #    hideMounts = true;
  #    allowOther = true;
  #    directories = [
  #      #xdg junk
  #      "Desktop"
  #      "Documents"
  #      "Downloads"
  #      "Games"
  #      "Music"
  #      "Templates"
  #      "Videos"

  #      #flatpak data
  #      ".var"

  #      #my stuff
  #      "based"
  #      "asmr"
  #      "repos"
  #      "randompersstuff"

  #      #dotstuff
  #      # ".ansible"
  #      ".gnupg"
  #      ".ssh"
  #      
  #      #TODO:see if anything else is needed here

  #    ];
  #  };
  };

}
