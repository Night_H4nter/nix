#!/usr/bin/env bash

#set -xv

# source ./config

#target drive(-s), mine is the default
TARGETDRIVE="/dev/disk/by-id/ata-CT500MX500SSD1_2244E67F59FF"
BPOOLNAME="bootpool"
RPOOLNAME="rootpool"


printf "Installing on %s\n" "$TARGETDRIVE."


printf "Disabling swap...\n"
swapoff -a

printf "Wiping all partitions and ZFS remains...\n"
zpool destroy -f "$BPOOLNAME" &> /dev/null
zpool destroy -f "$RPOOLNAME" &> /dev/null
# zpool labelclear "$TARGETDRIVE"
sgdisk --zap-all "$TARGETDRIVE" > /dev/null
dd if=/dev/zero of="$TARGETDRIVE" bs=512 count=1 status=none


# {{{
#create partitions
# for DRIVE in "${TARGETDRIVES[@]}"; do
#     sgdisk --zap-all "$DRIVE"
#     sgdisk -n1:1M:+1G -t1:EF00 "$DRIVE"
#     sgdisk -n2:0:+4G -t2:BE00 "$DRIVE"
#     sgdisk -n3:0:0   -t3:BF00 "$DRIVE"
# done

# }}}


set -e


printf "Creating partitions...\n"
sgdisk -n1:1M:+500M     -t1:EF00 "$TARGETDRIVE" > /dev/null
sgdisk -n2:0:+1G        -t2:BE00 "$TARGETDRIVE" > /dev/null
sgdisk -n3:0:0          -t3:BF00 "$TARGETDRIVE" > /dev/null

#until [ -e "$TARGETDRIVE-part3" ] ; do
printf "Waiting for udev to wake up and fill the /dev/disk/by-id/ directory...\n"
sleep 5
#done


printf "Creating an ESP...\n"
mkfs.fat -F32 -n "ESP" "$TARGETDRIVE-part1" > /dev/null

printf "Creating boot ZFS pool...\n"
zpool create -f \
    -o compatibility=grub2 \
    -o ashift=12 \
    -o autotrim=on \
    -O acltype=posixacl \
    -O compression=on \
    -O devices=off \
    -O normalization=formD \
    -O xattr=sa \
    -O relatime=on \
    -O canmount=off \
    -O mountpoint=none \
    "$BPOOLNAME" \
    "$TARGETDRIVE-part2"

printf "Creating root ZFS pool...\n"
zpool create -f \
    -o altroot="/mnt" \
    -o ashift=12 \
    -o autotrim=on \
    -O compression=on \
    -O acltype=posixacl \
    -O xattr=sa \
    -O normalization=formD \
    -O dnodesize=auto \
    -O sync=disabled \
    -O encryption=aes-256-gcm \
    -O keylocation=prompt \
    -O keyformat=passphrase \
    -O canmount=off \
    -O mountpoint=none \
    "$RPOOLNAME" \
    "$TARGETDRIVE-part3"

#create containers first, they are for systematization only
#temp is temporary, reset before every boot
#system is persistent, not backed up
#safe is persistent, backed up periodically
printf "Creating dataset containers...\n"
zfs create \
    -o atime=off \
    "$RPOOLNAME/temp"

zfs create \
    -o atime=off \
    "$RPOOLNAME/system"

zfs create \
    -o atime=off \
    "$RPOOLNAME/safe"

zfs create \
    -o relatime=on \
    "$BPOOLNAME/system"

printf "Creating datasets...\n"
zfs create \
    -o relatime=on \
    -o mountpoint=legacy \
    "$RPOOLNAME/temp/root"
zfs snapshot "$RPOOLNAME/temp/root@blank"

    #-o relatime=on \
    #-o mountpoint=legacy \
zfs create \
    -V 16G \
    "$RPOOLNAME/temp/swap"
zfs snapshot "$RPOOLNAME/temp/swap@blank"

zfs create \
    -o atime=off \
    -o mountpoint=legacy \
    "$RPOOLNAME/system/slashnix"

zfs create \
    -o relatime=on \
    -o mountpoint=legacy \
    "$RPOOLNAME/system/flatpak"

zfs create \
    -o relatime=on \
    -o mountpoint=legacy \
    "$RPOOLNAME/safe/persist"

zfs create \
    -o reservation=2G \
    "$RPOOLNAME/reserved"

zfs create \
    -o mountpoint=legacy \
    -o relatime=on \
    "$BPOOLNAME/system/slashboot"


printf "Mounting filesystems...\n"
mount -t zfs "$RPOOLNAME/temp/root" /mnt
mkdir -p /mnt/{boot,persist,nix,var/lib/flatpak}
mount -t zfs "$RPOOLNAME/system/slashnix" /mnt/nix
mount -t zfs "$BPOOLNAME/system/slashboot" /mnt/boot
mount -t zfs "$RPOOLNAME/safe/persist" /mnt/persist


printf "Setting up swap...\n"
mkswap "/dev/$RPOOLNAME/temp/swap"
swapon "/dev/$RPOOLNAME/temp/swap"


printf "This should be in the settings somewhere:
{
    boot.initrd.postDeviceCommands = lib.mkAfter ''
    zfs rollback -r %s/temp/root@blank
    zfs rollback -r %s/temp/swap@blank
    '';
}
" "$RPOOLNAME" "$RPOOLNAME"


nixos-generate-config --root /mnt
# rm "/mnt/etc/nixos/configuration.nix"
# cp "nixosconfig/flake/nixos/${userName}md/configuration.nix" "/mnt/etc/nixos/configuration.nix"

# nixos-install --no-root-passwd 
# umount -Rl /mnt
# zpool export -a
# reboot
