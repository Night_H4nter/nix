{ pkgs, ... }: {
  packages = with pkgs; [
    i3lock-color
    xkb-switch
  ];

  home.sessionPath = [ "$HOME/.local/bin/" ];

  home.file.screenLockerScript = {
    executable = true;
    source = ./screenlocker;
    desttination = ".local/bin/screenlocker";
  };

  services.screen-locker = {
    enable = true;
    inactiveInterval = 10;
    lockCmd = ".local/bin/screenlocker";
    xautolock = {
      enable = true;
      detectSleep = true;
    };
  };
}
