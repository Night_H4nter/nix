{ stable, ... }:
{
  xsession = {
    enable = true;
    windowManager.awesome = {
      enable = true;
      luaModules = with stable.luaPackages; [
        luarocks
      ];
    };
  };

  xdg.configFile.awesome = {
    source = ./config;
    recursive = true;
  };

  #tmp
  home.pkgs = with stable; [
    alacritty
    firefox
    git
  ];
}
